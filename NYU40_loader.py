import collections
import torch
import numpy as np
import scipy.misc as m
from torch.utils import data
import numbers

class NYU40Loader(data.Dataset):
    def __init__(self, root, split="test", is_transform=False, img_size=512):
        self.root = root
        self.split = split #https://www.dropbox.com/s/xa258pgvbmo6fb7/test.txt?dl=0
        self.is_transform = is_transform
        self.n_classes = 41
        self.img_size = img_size if isinstance(img_size, tuple) else (img_size, img_size)
        self.files = collections.defaultdict(list)
        file_list = tuple(open(root + split + '.txt', 'r'))
        file_list = [id_.rstrip() for id_ in file_list]
        self.files[split] = file_list

    def __len__(self):
        return len(self.files[self.split])

    def __getitem__(self, index):
        img_name = self.files[self.split][index]
        img_path = self.root + '/input/' + img_name + '.jpg' #https://www.dropbox.com/sh/fa2qos6hiog9zul/AACyTKAfJ1seL1X8Hb1Hh1UOa?dl=0
        dep_path = self.root + '/depths_float/' + img_name + '.tiff' #https://www.dropbox.com/sh/3stax2e0tshkzmc/AAAYQjT3fAZy8DXPsJCyCYkMa?dl=0

        img = m.imread(img_path)
        img = np.array(img, dtype=np.uint8)
        dep = m.imread(dep_path)

        if self.is_transform:
            img, dep = self.transform_test(img, dep)

        return img, dep

    def transform_test(self, img, dep):
        img = img.astype(float)
        hei,wid,_=img.shape
        img = m.imresize(img, (self.img_size[0], self.img_size[1]))
        img = img.astype(float) / 255.0
        dep = dep.astype(float)
        dep = m.imresize(dep, (self.img_size[0], self.img_size[1]),mode='F')
        dep = dep.astype(float)

        Crop = CenterCrop((228, 304))
        img, dep = Crop(img, dep)
        img = img.transpose(2, 0, 1)
        dep=m.imresize(dep,(480,640),mode='F')

        img = torch.from_numpy(img).float()
        dep = torch.from_numpy(dep).float()
        return img, dep

class CenterCrop(object):
    """Crops the given inputs and target arrays at the center to have a region of
    the given size. size can be a tuple (target_height, target_width)
    or an integer, in which case the target will be of a square shape (size, size)
    Careful, img1 and img2 may not be the same size
    """
    def __init__(self, size):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size

    def __call__(self, inputs, target_depth):
        h, w, _ = inputs.shape
        th, tw = self.size
        x = int(round((w - tw) / 2.))
        y = int(round((h - th) / 2.))

        inputs = inputs[y : y + th, x : x + tw]
        target_depth = target_depth[y : y + th, x : x + tw]

        return inputs,target_depth

if __name__ == '__main__':
    local_path = 'NYU2/data'
    dst = NYU40Loader(local_path, is_transform=True)
    trainloader = data.DataLoader(dst)
