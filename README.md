# Look Deeper into Depth: Monocular Depth Estimation with Semantic Booster and Attention-Driven Loss

By [Jianbo Jiao](https://jianbojiao.com/), [Ying Cao](http://www.ying-cao.com), [Yibing Song](https://ybsong00.github.io), [Rynson W. H. Lau](http://www.cs.cityu.edu.hk/~rynson/).


### Introduction

This repository contains the codes and models described in the paper "[Look Deeper into Depth: Monocular Depth Estimation with Semantic Booster and Attention-Driven Loss](https://jianbojiao.com/pdfs/eccv_LDiD.pdf)". This work addresses the problem of monocular depth estimation, with a specific focus on the long-tail distribution of the naturally captured depth data.

### Usage

0. The project was implemented and tested with Python 2.7, [PyTorch](https://pytorch.org) (version 0.3) and [TorchVision](https://pytorch.org/docs/0.2.0/) (version 0.2.0) on Linux with GPUs. Please setup the environment according to the [instructions](https://pytorch.org/get-started/previous-versions/) first.
1. Download the NYUD v2 dataset from the official [website](https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html) and store the RGB image and depth map to "dataset/img" and "dataset/dep" respectively and then store the depth map in .tiff format, or directly download the pre-processed data by running the script [dataset/data.sh](dataset/data.sh).
2. Download the model with the [script](models/model.sh) under folder "models".
3. Run the [main.py](main.py) to evaluate the performance of the proposed approach on the NYUD-v2 dataset for monocular depth estimation.

### Citation

If you find the code and model useful in your research, please cite:

	@InProceedings{ECCV18_LDiD,
		author = {Jianbo Jiao, Ying Cao, Yibing Song, Rynson Lau},
		title = {Look Deeper into Depth: Monocular Depth Estimation with Semantic Booster and Attention-Driven Loss},
		booktitle = {European Conference on Computer Vision (ECCV)},
		year = {2018}
	}

**References**

[1] Nathan Silberman, Derek Hoiem, Pushmeet Kohli and Rob Fergus. [Indoor Segmentation and Support Inference from RGBD Images](https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html). In Proceedings of the European Conference on Computer Vision 2012.



If you have any questions please email the [authors](mailto:jiaojianbo.i@gmail.com)
