import sys
import torch
import argparse
import numpy as np
import torch.nn as nn
import scipy.misc as misc
from torch.autograd import Variable

from NYU40_loader import *
import matplotlib.pyplot as plt

from model import *
import os

num_class=41

def compute_errors(gt, pred):
    thresh = np.maximum((gt / pred), (pred / gt))
    a1 = (thresh < 1.25   ).mean()
    a2 = (thresh < 1.25 ** 2).mean()
    a3 = (thresh < 1.25 ** 3).mean()

    rmse = (gt - pred) ** 2
    rmse = np.sqrt(rmse.mean())

    rmse_log = (np.log(gt) - np.log(pred)) ** 2
    rmse_log = np.sqrt(rmse_log.mean())

    abs_rel = np.mean(np.abs(gt - pred) / gt)

    log_10 = np.mean(np.abs(np.log10(gt) - np.log10(pred)))

    return abs_rel, log_10, rmse, rmse_log, a1, a2, a3

def accuracy(model,loader):
    num_samples=len(loader)
    rmse = np.zeros(num_samples, np.float32)
    rmse_log = np.zeros(num_samples,np.float32)
    abs_rel = np.zeros(num_samples, np.float32)
    log_10 = np.zeros(num_samples, np.float32)
    a1 = np.zeros(num_samples, np.float32)
    a2 = np.zeros(num_samples, np.float32)
    a3 = np.zeros(num_samples, np.float32)

    show=False
    fig=plt.figure()
    for i, (img, dep) in enumerate(loader):
        img_var = Variable(img.cuda(), volatile=True)
        pred_depths, pred_labels = model(img_var)

        preds_dep = np.squeeze(pred_depths.data.cpu().numpy())
        dep=np.squeeze(dep.cpu().numpy())
        #_, preds_lbl = pred_labels.data.cpu().max(1)
        #lbl=np.squeeze(lbl).numpy(); preds_lbl=np.squeeze(preds_lbl).numpy()
        #preds_lbl=misc.imresize(preds_lbl,lbl.shape,mode='F')

        if show:
            fig.suptitle("image %d/%d"%(i+1,num_samples))
            a=fig.add_subplot(3,1,1)
            img=img.cpu().numpy()
            img=img[0].transpose(1,2,0)
            plt.imshow(img)
            a.set_title('input')
            a=fig.add_subplot(3,1,2)
            plt.imshow(dep,cmap='jet')
            a.set_title('GT')
            a=fig.add_subplot(3,1,3)
            plt.imshow(preds_dep,cmap='jet',vmin=dep.min(),vmax=dep.max())
            a.set_title('pred')
            #lbl_0=(lbl==0)
            #preds_lbl[lbl_0]=0
            #a=fig.add_subplot(3,2,4)
            #plt.imshow(lbl,cmap='gist_ncar',vmin=0,vmax=num_class-1)
            #a=fig.add_subplot(3,2,6)
            #plt.imshow(preds_lbl,cmap='gist_ncar',vmin=0,vmax=num_class-1)
            plt.draw();plt.pause(0.01);plt.clf()
        else:
            print("processing %d/%d"%(i+1,num_samples))

        mask=(dep>0)
        abs_rel[i], log_10[i], rmse[i], rmse_log[i], a1[i], a2[i], a3[i] = compute_errors(dep[mask],preds_dep[mask])
        if np.isnan(rmse_log[i]):
            rmse_log[i]=0

    return  abs_rel.sum()/i,log_10.sum()/i,rmse.sum()/i,rmse_log.sum()/i,a1.sum()/i,a2.sum()/i,a3.sum()/i

def test(args):
    data_loader = NYU40Loader
    data_path = args.dataset
    loader = data_loader(data_path, is_transform=True, split="test",img_size=(240,320))
    testloader = data.DataLoader(loader)

    # Setup Model
    model=Model(ResidualBlock,UpProj_Block,1)
    state_dict = torch.load(args.model_path, map_location=lambda storage, loc: storage)
    model.load_state_dict(state_dict)
    if torch.cuda.is_available():
        model.cuda()
    model.eval()

    acc_dep1,acc_dep2 ,acc_dep3 ,acc_dep4 ,acc_dep5 ,acc_dep6, acc_dep7 = accuracy(model,testloader)
    print("Accuracy of depth is abs_rel(%f) log_10(%f) rmse(%f) rmse_log(%f) a1-a3(%f %f %f)\n"%\
          (acc_dep1,acc_dep2,acc_dep3,acc_dep4,acc_dep5,acc_dep6,acc_dep7))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Params')
    parser.add_argument('--model_path', nargs='?', type=str, default='model.pth',#https://www.dropbox.com/s/ubr74x9r3k927lf/model.pth?dl=0
                        help='Path to the saved model')
    parser.add_argument('--dataset', nargs='?', type=str, default='./dataset/',
                        help='Dataset to use')
    args = parser.parse_args()
    test(args)
